/*
 * @file: competition.js
 * @description: It Contain function layer for competition controller.
 * @author: Amarjit Singh
 */

import { successAction, failAction } from '../utilities/response';
import { addCompetition, getCompetition } from '../services/competition';
import Message from '../utilities/messages';

/**************** Add competition ***********/
export const add = async (req, res, next) => {
    const payload = req.body;
    try {
        const data = await addCompetition(payload);
        res.status(200).json(successAction(data, Message.registerSuccess));
    } catch (error) {
        res.status(400).json(failAction(error.message));
    }
};
/**************** Get Competition ***********/
export const get = async (req, res, next) => {
    try {
        const data = await getCompetition();
        res.status(200).json(successAction(data, Message.success));
    } catch (error) {
        res.status(400).json(failAction(error.message));
    }
};