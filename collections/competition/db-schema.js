/*
 * @file: db-schema.js
 * @description: It Contain db schema for user collection.
 * @author: Amarjit Singh
 */

import mongoose from 'mongoose';

const competitionSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true
  },
  description: {
    type: String,
    default: ''
  },
  startDate: {
    type: Date,
    required: true
  },
  endDate: {
    type: Date,
    required: true
  },
  status: {
    type: Number,
    default: 1 // 1 active, 2 deactive
  },
  users: {
    type: Number,
    default: 0,
  },
  price: {
    type: Number,
    default: 0,
  },
  createdAt: {
    type: Date,
    default: new Date(),
  },
  updatedAt: {
    type: Date,
    default: new Date(),
  }
});

export default competitionSchema;