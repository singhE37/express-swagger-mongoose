/*
 * @file: index.js
 * @description: It Contain function layer for user collection.
 * @author: Amarjit Singh
 */

import mongoose from 'mongoose';
import dbSchema from './db-schema';

class CompetitionClass {
  static add(payload) {
    return this(payload).save();
  }
  static findOneByCondition(condition) {
    return this.findOne(condition);
  }
  static findByCondition(condition) {
    return this.find(condition);
  }
}

dbSchema.loadClass(CompetitionClass);

export default mongoose.model('competitions', dbSchema);