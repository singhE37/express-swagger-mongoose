/*
 * @file: index.js
 * @description: It Contain function layer for user collection.
 * @author: Amarjit Singh
 */

import mongoose from 'mongoose';
import dbSchema from './db-schema';

class UserClass {
  static checkEmail(email) {
    return this.findOne({ email });
  }
  static findOneByCondition(condition) {
    return this.findOne(condition);
  }
  static checkUsername(username) {
    return this.findOne({ username });
  }
  static checkToken(token) {
    return this.findOne({ 'loginToken.token': token });
  }
  static saveUser(payload) {
    return this(payload).save();
  }
  static onLoginDone(userId, loginToken) {
    let updateData = {
      $push: { loginToken: { token: loginToken } },
      $set: {
        lastLogin: Date.now(),
        updatedAt: Date.now()
      }
    };

    return this.findByIdAndUpdate(userId, updateData, { new: true });
  }
  static logout(userId, token) {
    let updateData = {
      $set: {
        updatedAt: Date.now()
      },
      $pull: { loginToken: { token } }
    };
    return this.findByIdAndUpdate(userId, updateData);
  }
}

dbSchema.loadClass(UserClass);

export default mongoose.model('User', dbSchema);