/*
 * @file: db-schema.js
 * @description: It Contain db schema for user collection.
 * @author: Amarjit Singh
 */

import mongoose from 'mongoose';

const userSchema = new mongoose.Schema({
  username: {
    type: String,
    unique: true,
    required: true
  },
  email: {
    type: String,
    unique: true,
    required: true
  },
  password: {
    type: String,
    default: ''
  },
  loginToken: [
    {
      token: {
        type: String,
        required: true
      },
      createdAt: {
        type: Date,
        default: new Date(),
      }
    }
  ],
  status: {
    type: Number,
    default: 1 // 0 account removed, 1 active, 2 block
  },
  lastLogin: {
    type: Number,
    default: null,
  },
  role: {
    type: String,
    default: "user",
  },
  createdAt: {
    type: Date,
    default: new Date(),
  },
  updatedAt: {
    type: Date,
    default: new Date(),
  }
});

export default userSchema;