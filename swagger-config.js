/*
 * @file: swagger-config.js
 * @description: It Contain swagger configrations.
 * @author: Amarjit Singh
 */
import swaggerJsDocs from 'swagger-jsdoc';

const swaggerOptions = {
    swaggerDefinition: {
        info: {
            title: 'cassandra project',
            version: '1.0',
            description: 'All api end points',
            contact: {
                name: 'Amarjit Singh'
            },
            servers: ['http://localhost:3000']
        },
        produces: ["application/json"]
    },
    apis: [
        './api/user/*.js',
        './api/competition/*.js'
    ]
};
export default swaggerJsDocs(swaggerOptions);