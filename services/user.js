/*
 * @file: user.js
 * @description: It Contain function layer for user service.
 * @author: Amarjit Singh
 */

import User from '../collections/user';
import Message from '../utilities/messages';
import { encryptpassword, generateRandom } from '../utilities/universal';

export const save = async (payload) => {
    if (await User.checkUsername(payload.username)) throw new Error(Message.userNameAlreadyExists);
    if (await User.checkEmail(payload.email)) throw new Error(Message.emailAlreadyExists);
    if (payload['password']) {
        payload.password = encryptpassword(payload.password);
    }
    const userData = await User.saveUser({
        ...payload
    });
    const data = await User.onLoginDone(userData._id, generateRandom(50));
    return {
        _id: data._id,
        username: data.username,
        email: data.email,
        loginToken: data.loginToken[data.loginToken.length - 1].token,
        lastLogin: data.lastLogin
    };
}
export const onLogin = async (payload) => {
    const userData = await User.checkEmail(payload.email);
    if (!userData) throw new Error(Message.emailNotExists);
    if (payload['password'] && !await User.findOneByCondition({ email: payload.email, password: encryptpassword(payload.password) })) throw new Error(Message.invalidCredentials);
    if (userData.status === 0) throw new Error(Message.unauthorizedUser);
    if (userData.status === 2) throw new Error(Message.userBlocked);
    const data = await User.onLoginDone(userData._id, generateRandom(50));
    return {
        _id: data._id,
        username: data.username,
        email: data.email,
        loginToken: data.loginToken[data.loginToken.length - 1].token,
        lastLogin: data.lastLogin
    };
}