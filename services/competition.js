/*
 * @file: competition.js
 * @description: It Contain function layer for competition service.
 * @author: Amarjit Singh
 */

import Competition from '../collections/competition';
// import Message from '../utilities/messages';

export const addCompetition = async (payload) => {
    return await Competition.add({
        ...payload
    });
}
export const getCompetition = async () => {
    return await Competition.findByCondition({}).select({ __v: 0, createdAt: 0, updatedAt: 0 });
}