/*
 * @file: get.js
 * @description: It Contain all routers.
 * @author: Amarjit Singh
 */
import express from 'express';
import { get } from '../../controllers/competition';
import { checkToken } from '../../utilities/universal';
const app = express();

/**
 * @swagger
 * /api/v1/competition:
 *  get:
 *   tags: ["competition"]
 *   summary: get competition api
 *   description: api used to get competition
 *   security:
 *    - OAuth2: [admin]   # Use Authorization
 *   parameters:
 *      - in: header
 *        name: Authorization
 *        type: string
 *        required: true
 *   responses:
 *    '200':
 *    description: success
 */
app.get('/competition', checkToken, get);

export default app;