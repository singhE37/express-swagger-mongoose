/*
 * @file: add.js
 * @description: It Contain competition add router/api.
 * @author: Amarjit Singh
 */
import express from 'express';
import { createValidator } from 'express-joi-validation';
import Joi from '@hapi/joi';
import { add } from '../../controllers/competition';
import { checkToken } from '../../utilities/universal';
const app = express();
const validator = createValidator({ passError: true });
/**
 * @swagger
 * /api/v1/competition:
 *  post:
 *   tags: ["competition"]
 *   summary: add competition api
 *   description: api used to add competition
 *   security:
 *    - OAuth2: [admin]   # Use Authorization
 *   parameters:
 *      - in: header
 *        name: Authorization
 *        type: string
 *        required: true
 *      - in: body
 *        name: user
 *        description: The user to add competition.
 *        schema:
 *         type: object
 *         required:
 *          - competition add
 *         properties:
 *           name:
 *             type: string
 *             required:
 *           description:
 *             type: string
 *             optional:
 *           users:
 *             type: number
 *             required:
 *           price:
 *             type: number
 *             required:
 *           startDate:
 *             type: any
 *             required:
 *           endDate:
 *             type: any
 *             required:
 *   responses:
 *    '200':
 *    description: success
 */
const addCompetitionSchema = Joi.object({
    name: Joi.string()
        .required()
        .label("Competition name"),
    description: Joi.string()
        .optional()
        .allow("")
        .label("Description"),
    startDate: Joi.any()
        .required()
        .label("Start Date"),
    endDate: Joi.any()
        .required()
        .label("End Date"),
    users: Joi.number()
        .required()
        .label("Users"),
    price: Joi.number()
        .required()
        .label("Price")
});

app.post('/competition',
    validator.body(addCompetitionSchema, {
        joi: { convert: true, allowUnknown: false }
    }),
    checkToken, add);

export default app;