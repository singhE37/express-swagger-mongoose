/*
 * @file: index.js
 * @description: It's combine all competition routers.
 * @author: Amarjit Singh
 */

import add from './add';
import get from './get';

export default [add, get];
