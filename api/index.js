/*
 * @file: index.js
 * @description: It's combine all routers.
 * @author: Amarjit Singh
 */

import user from './user';
import competition from './competition';

/*********** Combine all Routes ********************/
export default [...user, ...competition];
