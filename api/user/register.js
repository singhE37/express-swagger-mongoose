/*
 * @file: login.js
 * @description: It Contain register router/api.
 * @author: Amarjit Singh
 */
import express from 'express';
import { createValidator } from 'express-joi-validation';
import Joi from '@hapi/joi';
import { register } from '../../controllers/user';
const app = express();
const validator = createValidator({ passError: true });

/**
 * @swagger
 * /api/v1/user/register:
 *  post:
 *   tags: ["user"]
 *   summary: user register api
 *   description: api used to register users
 *   parameters:
 *      - in: body
 *        name: user
 *        description: The user to create.
 *        schema:
 *         type: object
 *         required:
 *          - user signup
 *         properties:
 *           username:
 *             type: string
 *             required:
 *           email:
 *             type: string
 *             required:
 *   responses:
 *   "200":
 *    description: success
 */
const regiterSchema = Joi.object({
    username: Joi.string()
        .required()
        .label("User name"),
    email: Joi.string()
        .email()
        .required()
        .label("Email"),
    password: Joi.string()
        .optional()
        .label("Password"),
    role: Joi.string()
        .valid('user', 'admin')
        .optional()
        .label("Role")
});

app.post('/user/register'
    , validator.body(regiterSchema, {
        joi: { convert: true, allowUnknown: false }
    }), register);

export default app;