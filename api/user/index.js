/*
 * @file: index.js
 * @description: It's combine all user routers.
 * @author: Amarjit Singh
 */

import regiter from './register';
import login from './login';

export default [regiter, login];
