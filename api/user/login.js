/*
 * @file: login.js
 * @description: It Contain login router/api.
 * @author: Amarjit Singh
 */
import express from 'express';
import { createValidator } from 'express-joi-validation';
import Joi from '@hapi/joi';
import { login } from '../../controllers/user';
const app = express();
const validator = createValidator({ passError: true });
// https://swagger.io/docs/specification/2-0/describing-parameters

/**
 * @swagger
 * /api/v1/user/login:
 *  post:
 *   tags: ["user"]
 *   summary: user login api
 *   description: api used to login users
 *   parameters:
 *      - in: body
 *        name: user
 *        description: The user to login.
 *        schema:
 *         type: object
 *         required:
 *          - user login
 *         properties:
 *           email:
 *             type: string
 *             required:
 *           password:
 *             type: string
 *             optional:
 *   responses:
 *    "200":
 *    description: success
 */

const loginSchema = Joi.object({
    email: Joi.string()
        .email()
        .required()
        .label("Email"),
    password: Joi.string()
        .optional()
        .label("Password")
});

app.post('/user/login', validator.body(loginSchema, {
    joi: { convert: true, allowUnknown: false }
}), login);

export default app;